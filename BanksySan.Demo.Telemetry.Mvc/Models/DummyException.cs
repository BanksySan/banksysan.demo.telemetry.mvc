﻿namespace BanksySan.Demo.Telemetry.Mvc.Models
{
    using System;

    public class DummyException : Exception
    {
        public DummyException(string message)
            : base(message)
        {
        }
    }
}