﻿namespace BanksySan.Demo.Telemetry.Mvc.Controllers
{
    using System.Web.Mvc;
    using Models;

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ThrowException(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                throw new DummyException("dummy-exception");
            }
            throw new DummyException(message);
        }

        public ActionResult Echo(string message)
        {
            ViewBag.Message = message;

            return View();
        }
    }
}