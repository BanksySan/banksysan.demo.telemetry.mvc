﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BanksySan.Demo.Telemetry.Mvc.Filters
{
    using System.Web.Mvc;
    using Microsoft.ApplicationInsights;

    public class TelemeterExceptionFilter : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            var telemetryClient = new TelemetryClient();

            telemetryClient.TrackException(filterContext.Exception);

            base.OnException(filterContext);
        }
    }
}